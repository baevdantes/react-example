import axiosInstance from './axios.instance';

export default {
  getWorkouts() {
    return axiosInstance.get('/workouts');
  },

  getWorkoutById(id) {
    return axiosInstance.get('/workouts/' + id);
  },

  updateWorkout(id, data) {
    return axiosInstance.put('/workouts/' + id, data);
  },

  createNewWorkout(data) {
    return axiosInstance.post('/workouts', data);
  },

  deleteWorkout(id) {
    return axiosInstance.delete('/workouts/' + id);
  }
};
