import { Container, FormControlLabel, Switch, Typography } from '@mui/material';
import { DataContext } from '../App';
import { useContext, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleTheme } from '../store/themeSlice';

export default function Settings() {
  const { title } = useContext(DataContext);
  const dispatch = useDispatch();
  const darkMode = useSelector((state) => state.theme.darkMode);

  const onToggleTheme = () => {
    dispatch(toggleTheme());
  };

  useEffect(() => {
    document.title = `Settings - ${title}`;
  }, []);

  return (
    <Container sx={{ mt: 2 }}>
      <Typography sx={{ mb: 2 }} variant={'h3'}>
        Settings
      </Typography>
      <FormControlLabel
        control={<Switch onChange={() => onToggleTheme()} checked={darkMode} />}
        label="Dark mode"
      />
    </Container>
  );
}
