import {
  Box,
  Button,
  CircularProgress,
  Container,
  Grid,
  IconButton,
  List,
  ListItem,
  TextField,
  Typography
} from '@mui/material';
import { useLocation, useNavigate, useParams } from 'react-router-dom';
import { Fragment, useContext, useEffect, useMemo, useState } from 'react';
import api from '../api';
import { DataContext } from '../App';
import { Add, DeleteOutline } from '@mui/icons-material';
import { createWorkoutRoute } from '../types';

export default function WorkoutDetails() {
  const { id } = useParams();
  const location = useLocation();
  const navigate = useNavigate();
  const [creationPage, setCreationPageFlag] = useState(true);
  const [workout, setWorkout] = useState(null);
  const [exercises, setExercises] = useState([]);
  const [loading, setLoading] = useState(true);
  const [loadingUpdate, setLoadingUpdate] = useState(false);
  const { title } = useContext(DataContext);
  const getWorkout = () => {
    setLoading(true);
    api
      .getWorkoutById(id)
      .then((res) => {
        setWorkout(res.data);
        setExercises(res.data.exercises);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (location.pathname === createWorkoutRoute) {
      setLoading(false);
      setCreationPageFlag(true);
      setWorkout({
        date: new Date().toDateString()
      });
      setExercises([
        {
          count: '',
          title: ''
        }
      ]);
      document.title = `New workout - ${title}`;
    } else {
      setCreationPageFlag(false);
      document.title = `Workout#${id} - ${title}`;
      getWorkout();
    }
  }, []);

  const addExerciseLine = () => {
    setExercises([...exercises, { count: '', title: '' }]);
  };

  const deleteWorkout = () => {
    setLoadingUpdate(true);
    api
      .deleteWorkout(id)
      .then(() => {
        navigate('/');
      })
      .finally(() => {
        setLoadingUpdate(false);
      });
  };

  const onChangeValue = (event, index, field) => {
    const foundExercise = exercises[index];
    const newExercises = exercises.map((e, index) => {
      if (foundExercise && exercises.indexOf(foundExercise) === index) {
        return {
          ...foundExercise,
          [field]: event.target.value
        };
      }
      return e;
    });
    setExercises(newExercises);
  };

  const disabledToSave = useMemo(() => {
    return exercises.some((e) => {
      return !e.title || !e.count;
    });
  }, [exercises]);

  useEffect(() => {
    setCreationPageFlag(
      !!location && !!location.pathname && location.pathname === createWorkoutRoute
    );
  }, [location.pathname]);

  const updateWorkout = () => {
    setLoadingUpdate(true);
    if (creationPage) {
      api
        .createNewWorkout({
          ...workout,
          exercises
        })
        .then((res) => {
          navigate('/workout/' + res.data.id);
        })
        .finally(() => {
          setLoadingUpdate(false);
        });
    } else {
      api
        .updateWorkout(id, {
          ...workout,
          exercises
        })
        .then((res) => {
          setExercises(res.data.exercises);
        })
        .finally(() => {
          setLoadingUpdate(false);
        });
    }
  };

  const removeExercise = (exerciseIndex) => {
    const newExercises = exercises.filter((e, index) => index !== exerciseIndex);
    setExercises(newExercises);
  };

  const AddWorkout = (props) => {
    const { index } = props;
    if (index === exercises.length - 1) {
      return (
        <Grid item xs={1}>
          <IconButton disabled={loadingUpdate} onClick={() => addExerciseLine()}>
            <Add color="primary" />
          </IconButton>
        </Grid>
      );
    }
  };

  if (!loading) {
    return (
      <Box>
        <Container sx={{ mt: 2 }}>
          <Fragment>
            {creationPage ? (
              <Typography variant="h3">New workout</Typography>
            ) : (
              <Typography variant="h3">Workout #{id}</Typography>
            )}
            <Typography sx={{ mt: 1, mb: 2 }} variant="subtitle1">
              {workout.date}
            </Typography>
            {exercises.length ? (
              <List component={'form'}>
                {exercises.map((e, idx) => {
                  return (
                    <ListItem sx={{ paddingX: 0 }} key={idx}>
                      <Grid sx={{ alignItems: 'center' }} container spacing={3}>
                        <Grid item xs={6}>
                          <TextField
                            disabled={loadingUpdate}
                            label="Title"
                            key={['titleField', idx].join('_')}
                            fullWidth
                            type="text"
                            value={e.title}
                            onChange={(event) => onChangeValue(event, idx, 'title')}
                          />
                        </Grid>
                        <Grid item xs={4}>
                          <TextField
                            disabled={loadingUpdate}
                            type={'number'}
                            fullWidth
                            label="Count"
                            value={e.count}
                            onChange={(event) => onChangeValue(event, idx, 'count')}
                          />
                        </Grid>
                        <Grid item xs={1}>
                          <IconButton disabled={loadingUpdate} onClick={() => removeExercise(idx)}>
                            <DeleteOutline color="error" />
                          </IconButton>
                        </Grid>
                        <AddWorkout index={idx} />
                      </Grid>
                    </ListItem>
                  );
                })}
              </List>
            ) : (
              <Typography sx={{ marginY: 2 }}>No exercises</Typography>
            )}
            <Grid sx={{ alignItems: 'center' }} container spacing={3}>
              {exercises.length ? (
                <Grid item xs={3}>
                  <Button
                    variant="contained"
                    size="medium"
                    fullWidth
                    disabled={loadingUpdate || disabledToSave}
                    onClick={() => updateWorkout()}>
                    {creationPage ? 'Add' : 'Save'}
                  </Button>
                </Grid>
              ) : null}
              {!exercises.length ? (
                <Grid item xs={3}>
                  <Button
                    variant="outlined"
                    size="medium"
                    fullWidth
                    disabled={loadingUpdate}
                    onClick={() => addExerciseLine()}>
                    Add exercise
                  </Button>
                </Grid>
              ) : null}
              <Grid item xs={2}>
                <Button
                  size="medium"
                  fullWidth
                  color={'error'}
                  variant={'outlined'}
                  disabled={loadingUpdate}
                  onClick={() => deleteWorkout()}>
                  Delete
                </Button>
              </Grid>
            </Grid>
          </Fragment>
        </Container>
      </Box>
    );
  } else {
    return (
      <Box sx={{ display: 'flex', mt: 2, alignItems: 'center', justifyContent: 'center' }}>
        <CircularProgress />
      </Box>
    );
  }
}
