import { Button, Container, Grid, Typography } from '@mui/material';
import { useDispatch, useSelector } from 'react-redux';
import WorkoutCard from '../components/WorkoutCard';
import { useContext, useEffect, useState } from 'react';
import api from '../api';
import { setWorkouts } from '../store/workoutsSlice';
import { useNavigate } from 'react-router-dom';
import { DataContext } from '../App';

export default function Workouts() {
  const workoutsStore = useSelector((state) => state.workouts);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(true);
  const { title } = useContext(DataContext);

  useEffect(() => {
    getWorkouts();
    document.title = `Workouts - ${title}`;
  }, []);

  const selectWorkout = (workout) => {
    navigate('/workout/' + workout.id);
  };

  const createWorkout = () => {
    navigate('/workout/new');
  };

  const getWorkouts = () => {
    setLoading(true);
    api
      .getWorkouts()
      .then((res) => {
        dispatch(setWorkouts(res.data));
      })
      .finally(() => {
        setLoading(false);
      });
  };

  return (
    <Container sx={{ mt: 2 }}>
      <Typography variant={'h3'} sx={{ mb: 2 }}>
        Workouts
      </Typography>
      <WorkoutsList
        onSelectWorkout={selectWorkout}
        skeleton={
          <Grid sx={{ mb: 2 }} container spacing={2}>
            {[...Array(6).keys()].map((w) => (
              <Grid key={w} item xs={12} sm={6} md={4}>
                <WorkoutCard loading={loading} />
              </Grid>
            ))}
          </Grid>
        }
        empty={
          <Grid sx={{ mb: 2 }} alignContent="center" justifyContent="center">
            <Typography>No workouts yet</Typography>
          </Grid>
        }
        workoutsList={
          <Grid sx={{ mb: 2 }} container spacing={2}>
            {workoutsStore.workoutsList.map((w) => (
              <Grid key={w.id} item xs={12} sm={6} md={4}>
                <WorkoutCard onSelectWorkout={() => selectWorkout(w)} loading={loading} info={w} />
              </Grid>
            ))}
          </Grid>
        }
        items={workoutsStore.workoutsList}
        loading={loading}
      />
      <Grid container spacing={3}>
        <Grid item xs={3}>
          <Button variant="contained" onClick={() => createWorkout()}>
            Add workout
          </Button>
        </Grid>
      </Grid>
    </Container>
  );
}

export const WorkoutsList = (props) => {
  if (props.loading) {
    return props.skeleton;
  } else if (props.items.length) {
    return props.workoutsList;
  }
  return props.empty;
};
