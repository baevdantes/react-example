import { Box, Button, Container, Grid, Typography } from '@mui/material';
import { useNavigate } from 'react-router-dom';

export default function NotFound() {
  const navigate = useNavigate();
  const goHome = () => {
    navigate('/');
  };

  return (
    <Box>
      <Container>
        <Grid container direction="column" justifyContent="center" alignItems="center">
          <Typography variant="h2" component="h1">
            Not found
          </Typography>
          <Button onClick={goHome}>Go to home page</Button>
        </Grid>
      </Container>
    </Box>
  );
}
