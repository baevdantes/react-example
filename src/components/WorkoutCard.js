import { Card, CardContent, Skeleton, Typography } from '@mui/material';

export default function WorkoutCard(props) {
  return (
    <Card
      onClick={() => (props.loading ? null : props.onSelectWorkout())}
      sx={{ cursor: 'pointer' }}
      variant={'elevation'}
      elevation={2}>
      <CardContent>
        <Typography variant="h6" component="p">
          {props.loading ? <Skeleton width={130} /> : props.info.date}
        </Typography>
      </CardContent>
    </Card>
  );
}
