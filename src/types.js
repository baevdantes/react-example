export const darkThemeType = 'dark';
export const lightThemeType = 'light';
export const appTitle = 'FitApp';
export const darkModeLS = 'darkModeLS';
export const createWorkoutRoute = '/workout/new';
