import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { createTheme, CssBaseline, ThemeProvider } from '@mui/material';
import MainLayout from './layouts/MainLayout';
import Workouts from './routes/Workouts';
import NotFound from './routes/NotFound';
import Settings from './routes/Settings';
import EmptyLayout from './layouts/EmptyLayout';
import WorkoutDetails from './routes/WorkoutDetails';
import { createContext, useEffect } from 'react';
import { appTitle, createWorkoutRoute, darkModeLS, darkThemeType, lightThemeType } from './types';
import { useDispatch, useSelector } from 'react-redux';
import { setDarkMode } from './store/themeSlice';

export const DataContext = createContext({
  title: appTitle
});

export default function App() {
  const darkMode = useSelector((state) => state.theme.darkMode);
  const dispatch = useDispatch();
  const theme = createTheme({
    palette: {
      mode: darkMode ? darkThemeType : lightThemeType
    }
  });

  useEffect(() => {
    if (localStorage.getItem(darkModeLS) && Boolean(JSON.parse(localStorage.getItem(darkModeLS)))) {
      dispatch(setDarkMode());
    }
  }, []);

  return (
    <DataContext.Provider value={{ title: appTitle }}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <BrowserRouter>
          <Routes>
            <Route element={<MainLayout />}>
              <Route index element={<Workouts />} />
              <Route path="/settings" element={<Settings />} />
              <Route path="/workout/:id" element={<WorkoutDetails />} />
              <Route path={createWorkoutRoute} element={<WorkoutDetails />} />
            </Route>
            <Route element={<EmptyLayout />}>
              <Route path="*" element={<NotFound />} />
            </Route>
          </Routes>
        </BrowserRouter>
      </ThemeProvider>
    </DataContext.Provider>
  );
}
