import { createSlice } from '@reduxjs/toolkit';

export const workoutsSlice = createSlice({
  name: 'workouts',
  initialState: {
    workoutsList: []
  },
  reducers: {
    setWorkouts: (state, action) => {
      state.workoutsList = action.payload;
    }
  }
});

export const { setWorkouts } = workoutsSlice.actions;
