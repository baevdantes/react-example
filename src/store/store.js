import { configureStore } from '@reduxjs/toolkit';
import { workoutsSlice } from './workoutsSlice';
import { themeSlice } from './themeSlice';

export default configureStore({
  reducer: {
    workouts: workoutsSlice.reducer,
    theme: themeSlice.reducer
  }
});
