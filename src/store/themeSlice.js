import { createSlice } from '@reduxjs/toolkit';
import { darkModeLS } from '../types';

export const themeSlice = createSlice({
  initialState: {
    darkMode: false
  },
  name: 'theme',
  reducers: {
    toggleTheme: (state) => {
      state.darkMode = !state.darkMode;
      localStorage.setItem(darkModeLS, JSON.stringify(state.darkMode));
    },
    setDarkMode: (state) => {
      state.darkMode = true;
      localStorage.setItem(darkModeLS, JSON.stringify(true));
    }
  }
});

export const { toggleTheme } = themeSlice.actions;
export const { setDarkMode } = themeSlice.actions;
