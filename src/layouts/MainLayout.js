import {
  AppBar,
  Box,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography
} from '@mui/material';
import { Outlet, useNavigate } from 'react-router-dom';
import { DarkMode, FitnessCenter, LightMode, Menu, Settings } from '@mui/icons-material';
import { useContext, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { toggleTheme } from '../store/themeSlice';
import { DataContext } from '../App';

const pages = [
  {
    path: '/',
    label: 'Workouts',
    icon: <FitnessCenter />
  },
  {
    path: '/settings',
    label: 'Settings',
    icon: <Settings />
  }
];

export default function MainLayout() {
  const [isOpenDrawer, setDrawerState] = useState(false);
  const darkMode = useSelector((state) => state.theme.darkMode);
  const dispatch = useDispatch();
  const { title } = useContext(DataContext);

  const navigate = useNavigate();

  const onClickItem = (item) => {
    navigate(item.path);
    setDrawerState(false);
  };

  return (
    <Box>
      <AppBar position={'sticky'}>
        <Toolbar>
          <IconButton onClick={() => setDrawerState(true)} color={'inherit'} sx={{ mr: 1 }}>
            <Menu />
          </IconButton>
          <Typography sx={{ flexGrow: 1 }} variant={'h5'}>
            {title}
          </Typography>
          <IconButton onClick={() => dispatch(toggleTheme())}>
            {darkMode ? <LightMode /> : <DarkMode />}
          </IconButton>
        </Toolbar>
      </AppBar>
      <Drawer open={isOpenDrawer} onClose={() => setDrawerState(false)}>
        <Box width="250px">
          <List>
            <ListItem>
              <ListItemText>
                <Typography variant="h5">{title}</Typography>
              </ListItemText>
            </ListItem>
            {pages.map((item) => (
              <ListItem onClick={() => onClickItem(item)} button key={item.path}>
                <ListItemIcon>{item.icon}</ListItemIcon>
                <ListItemText>{item.label}</ListItemText>
              </ListItem>
            ))}
          </List>
        </Box>
      </Drawer>
      <Outlet />
    </Box>
  );
}
